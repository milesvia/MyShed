package com.example.myapplication;

import android.app.Application;
import android.content.Context;

import androidx.room.Room;

import com.example.myapplication.database.appDatabase.AppDatabase;

import java.io.File;

public class App extends Application {
    public static App instance;
    private AppDatabase database;
    public static boolean createDatabase;

    @Override
    public void onCreate() {
        super.onCreate();

        createDatabase=doesDatabaseExist(this, "database");
        instance = this;
        database = Room.databaseBuilder(this, AppDatabase.class, "database")
                .build();

    }
    public  AppDatabase getDatabase() {
        return database;
    }
    public static App getInstance() { return instance; }

    private static boolean doesDatabaseExist(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        return dbFile.exists();
    }
}
