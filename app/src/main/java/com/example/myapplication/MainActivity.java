package com.example.myapplication;


import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.content.Intent;
import android.net.Uri;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Toast;
import com.example.myapplication.adapter.ListItemShedApapter;
import com.example.myapplication.database.callback.DeleteAlltDatabaseShedComplete;
import com.example.myapplication.database.callback.InsertDatabaseShedComplete;
import com.example.myapplication.database.callback.RequestDatabaseShedComplete;
import com.example.myapplication.database.enity.ItemShed;
import com.example.myapplication.database.select.SelectItemShed;
import com.example.myapplication.utils.FileUtil;
import com.example.myapplication.utils.OpenManagerFileComplete;
import com.example.myapplication.utils.TreatmentExelFile;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import static com.example.myapplication.App.createDatabase;

public class MainActivity extends AppCompatActivity implements InsertDatabaseShedComplete,
        DeleteAlltDatabaseShedComplete, RequestDatabaseShedComplete,
        OpenManagerFileComplete {
   //раскрывающийся список для выбора группы
    private myAutoComplete autoCompleteTextViewGroup;
    private final String[] allGroups = { "701", "702"};

    //раскрывающийся список для выбора дня недели
    private myAutoComplete autoCompleteTextViewWeeksDay;
    private final String[] weeksDay = { "Понедельник", "Вторник", "Среда", "Четверг",
            "Пятница", "Суббота" };
    //позиция выбранного дня недели, которая соотвествует номеру дня недели
    private final int[] positionDayOfWeek = {0};

    //адаптер для расписания
    ListItemShedApapter listItemShedApapter;
    //куда будет отображаться расписание
    RecyclerView recyclerView;

    //кнопка круговая стрелка - обновить расписания
    ImageButton updateDatabase;
    //кнопка скрепка - загрузить из собственного файла
    ImageButton openNewExelFule;

    TreatmentExelFile parserXeleFile;
    FileUtil fileUtil=new FileUtil();
    SelectItemShed selectItemShed;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        parserXeleFile=new TreatmentExelFile();
        fileUtil=new FileUtil();
        selectItemShed=new SelectItemShed();

        //анимация "мерцания" для кнопок "обновить расписания" и "загрузить из своего файла"
        final Animation animAlpha = AnimationUtils.loadAnimation(this, R.anim.alpha);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        //ставим соотвествие xml-файлу
        openNewExelFule=findViewById(R.id.open_managerAndroid);
        updateDatabase=findViewById(R.id.updateDatabase);
        updateDatabase.setOnClickListener(v-> v.startAnimation(animAlpha));
        //клик по кнопке скрепка
        openNewExelFule.setOnClickListener(v-> {
            //мерцание,что нажата кнопка
            v.startAnimation(animAlpha);
            //вызов менеджера файлов
            fileUtil.openManagerFiles(this);});

        autoCompleteTextViewGroup = findViewById(R.id.autoCompleteTextViewGroup);
        autoCompleteTextViewGroup.setAdapter(new ArrayAdapter<>(this, R.layout.item_autotextview, allGroups));
        //первоночально ставим первую запись и списка
        autoCompleteTextViewGroup.setText(allGroups[0]);
        //клип по выбранной групе
        autoCompleteTextViewGroup.setOnItemClickListener((parent, view, position, id) -> {
            //обновляем расписание в соотвествие с новыми данными
            updateRecyclerView();
        });

        autoCompleteTextViewWeeksDay = findViewById(R.id.autoCompleteTextViewWeeksDay);
        autoCompleteTextViewWeeksDay.setAdapter(new ArrayAdapter<>(this,
                R.layout.item_autotextview, weeksDay));
        //первоночально ставим первую запись и списка
        autoCompleteTextViewWeeksDay.setText(weeksDay[0]);
        //клип по выбранному дню недели
        autoCompleteTextViewWeeksDay.setOnItemClickListener((parent, view, position, id) -> {
            //обновляем расписание в соотвествие с новыми данными
            positionDayOfWeek[0] =position;
            updateRecyclerView();
        });

        //отображение расписания
        recyclerView=findViewById(R.id.id_recycler_shed);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        //проверка есть ли пзаписи в базе данных
        if(!createDatabase) {
            //если нет данных, то спарсим exel файл и добавляем данные
            InputStream stream = getApplicationContext().getResources().openRawResource(R.raw.test1);
             addNewShed(stream);
         } else updateRecyclerView(); //иначе отображае имеющиеся

    }


    //срабатывает, когда закрывается окно Менеджер файлов
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //если был открыт менеджер файлов и файл был выбран
        if (requestCode == 123 && resultCode == RESULT_OK) {
            if ((data != null) && (data.getData() != null)) {
                Uri selectedFile = data.getData();
                try {
                    //отправка на парсинг файла
                    InputStream stream= getContentResolver().openInputStream(selectedFile);
                    addNewShed(stream);
                } catch (FileNotFoundException e) {
                    //файл поврежден
                    Toast.makeText(getApplicationContext(),"Не удалось открыть файл :(", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }
    }

    //обновить записи расписания
    private void updateRecyclerView() {
        //считываем, какую группу и день недели выбрал пользователь
        String choseGroup=autoCompleteTextViewGroup.getText().toString();
        try{
            int group=Integer.parseInt(choseGroup);
            System.out.println("User chose group №"+group+" is "+choseGroup);
            int day=positionDayOfWeek[0];
            System.out.println("User chose day of week №"+group+" is "+weeksDay[day]);
            //запрос в базу данных, который вернет записи выбранной группы и выбранном дню недели
            selectItemShed.getItemShedForGroupANDDayOfWeek(group,day ,this);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Данные заданы некорректно.\nПожалуйста выберите из списка группу и день недели", Toast.LENGTH_SHORT).show();
        }
    }

    //новое создание расписания
    private void addNewShed(InputStream stream){
        //парсим выбранной файл InputStream stream
        ArrayList<ItemShed> thisShed=(ArrayList<ItemShed>) parserXeleFile.openAndReadMainExelFile(stream);
        if(thisShed!=null) {
            //запрос в базу данных на удаление всех записей
            selectItemShed.deleteAllShedDatabase(this);
            //запрос в базу данных на добавление новых записей thisShed2
            selectItemShed.insertListItemsShedDatabase(thisShed, this);
        }
        else{
            //если exel файл пустой или поврежден
            Toast.makeText(getApplicationContext(), "Не удалось распарсить данные :(", Toast.LENGTH_SHORT).show();
        }
    }

    //объявление view для отображения расписания
    private void initRecyclerShed(ArrayList<ItemShed> thisShed){
        listItemShedApapter=new ListItemShedApapter(getApplicationContext(), thisShed);
        recyclerView.setAdapter(listItemShedApapter);
    }

    @Override
    public void addItemShedsComplete(ArrayList<ItemShed> itemSheds) {
        //ждем пока все записи удалятся из базы данных чтобы избежать RaceCondition
        while (!flagCompleteDelete);
        this.runOnUiThread(() -> {
            Toast.makeText(getApplicationContext(), "Расписание успешно сохранено на устройство", Toast.LENGTH_SHORT).show();
            updateRecyclerView();
        });
    }

    //флаг который помогает понять что поток отработал
    private boolean flagCompleteDelete=false;
    @Override
    public void deleteAllItemShedsComplete() {
        flagCompleteDelete=true;
    }


    @Override
    public void getItemShedsCompleteForDayOfWeekANDThisGroup(ArrayList<ItemShed> itemSheds) {
        //отображаем в главном потоке полученные данные
        this.runOnUiThread(() ->initRecyclerShed(itemSheds));
    }


    @Override
    public void openManagFiles(Intent intent) {
        //открыть менеджер файлов
        startActivityForResult(Intent.createChooser(intent, "Select a file"), 123);
    }
}