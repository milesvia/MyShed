package com.example.myapplication.database.Dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.myapplication.database.enity.ItemShed;

import java.util.List;

@Dao
public interface ShedDao {
    // Добавление записи в бд
    @Insert
    long insertItemShed(ItemShed field);

    // Добавление записей в бд
    @Insert
    void insertItemSheds(List<ItemShed> fields);

    // Удаление записи из бд
    @Delete
    void deleteItemShed(ItemShed field);

    // Удаление всей таблицы  из бд
    @Query("DELETE FROM ItemShed")
    void deleteAll();

    // Удаление записей из бд
    @Delete
    void deleteItemSheds(List<ItemShed> itemSheds);
    // Получение всех записей из бд для определенного дня недели
    @Query("SELECT * FROM itemshed")
    List<ItemShed> getItemShed();

    // Получение всех записей из бд для определенного дня недели
    @Query("SELECT * FROM itemshed WHERE itemshed.dayOfWeek = :dayOfWeek")
    List<ItemShed> getItemShedWitThisOfDay(int dayOfWeek);

    // Получение всех записей из бд для определенного дня недели
    @Query("SELECT * FROM itemshed WHERE itemshed.`group` = :group")
    List<ItemShed> getItemShedForThisGroup(int group);

    // Получение всех записей из бд для определенного дня недели и группы
    @Query("SELECT * FROM itemshed WHERE itemshed.`group` = :group AND itemshed.dayOfWeek = :dayOfWeek")
    List<ItemShed> getItemShedForThisGroupAndDayOfWeek(int group, int dayOfWeek);


}

