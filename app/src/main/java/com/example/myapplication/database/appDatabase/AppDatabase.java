package com.example.myapplication.database.appDatabase;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.myapplication.database.Dao.ShedDao;
import com.example.myapplication.database.enity.ItemShed;

@Database(entities =  ItemShed.class , version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ShedDao shedDao();
}
