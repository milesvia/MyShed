package com.example.myapplication.database.callback;

import com.example.myapplication.database.enity.ItemShed;

import java.util.ArrayList;
import java.util.List;

public interface InsertDatabaseShedComplete {
    void addItemShedsComplete(ArrayList<ItemShed> itemSheds);
}
