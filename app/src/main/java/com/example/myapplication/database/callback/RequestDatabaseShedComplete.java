package com.example.myapplication.database.callback;

import com.example.myapplication.database.enity.ItemShed;

import java.util.ArrayList;
import java.util.List;

public interface RequestDatabaseShedComplete {
    void getItemShedsCompleteForDayOfWeekANDThisGroup(ArrayList<ItemShed> itemSheds);
}
