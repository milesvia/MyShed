package com.example.myapplication.database.enity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

//структура таблицы Расписание в базе данных
@Entity
public class ItemShed {
    @PrimaryKey(autoGenerate = true)
    private long id;

    private String time;
    private String object;
    private String address;
    private String teacher;

    private int group;
    private int dayOfWeek;

    public ItemShed(){
    }

    public ItemShed(String time, String object, String address, String teacher, int group, int dayOfWeek){
        this.time=time;
        this.object=object;
        this.address=address;
        this.teacher=teacher;
        this.group=group;
        this.dayOfWeek=dayOfWeek;
    }
    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int getDayOfWeek() { return dayOfWeek; }

    public void setDayOfWeek(int dayOfWeek) { this.dayOfWeek = dayOfWeek; }

}
