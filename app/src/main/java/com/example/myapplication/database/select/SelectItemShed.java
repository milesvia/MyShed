package com.example.myapplication.database.select;

import android.os.AsyncTask;
import com.example.myapplication.App;
import com.example.myapplication.database.Dao.ShedDao;
import com.example.myapplication.database.appDatabase.AppDatabase;
import com.example.myapplication.database.callback.DeleteAlltDatabaseShedComplete;
import com.example.myapplication.database.callback.InsertDatabaseShedComplete;
import com.example.myapplication.database.callback.RequestDatabaseShedComplete;
import com.example.myapplication.database.enity.ItemShed;

import java.util.ArrayList;

//запросы в базу данных
public class SelectItemShed {
    public SelectItemShed(){}
    //вставить в базу данных несколько строчек расписания ArrayList<ItemShed> itemsShed
    public void insertListItemsShedDatabase(ArrayList<ItemShed> itemsShed, InsertDatabaseShedComplete insertDatabaseShedComplete) {
        //в отдельном потоке посылаем запрос в бд
        AsyncTask.execute(() -> {
            //получаем текущее состояние бд
            AppDatabase db = App.getInstance().getDatabase();
            ShedDao shedDao=db.shedDao();
            if(itemsShed==null)
                return;
            //соотвествующий запрос
            shedDao.insertItemSheds(itemsShed);
            //отправка в соответсвующий callback по завершению работы потока
            insertDatabaseShedComplete.addItemShedsComplete(itemsShed);
        });
    }

    //удалить из базы данных все записи
    public void deleteAllShedDatabase(DeleteAlltDatabaseShedComplete deleteAlltDatabaseShedComplete) {
        //в отдельном потоке посылаем запрос в бд
        AsyncTask.execute(() -> {
            //получаем текущее состояние бд
            AppDatabase db = App.getInstance().getDatabase();
            ShedDao shedDao=db.shedDao();
            //соотвествующий запрос
            shedDao.deleteAll();
            //отправка в соответсвующий callback по завершению работы потока
            deleteAlltDatabaseShedComplete.deleteAllItemShedsComplete();
        });
    }

    //отправляем запрос в бд на получение тех стрчое красписания которые соотвествует дню недели dayOfWeek и номеру группы group
    public void getItemShedForGroupANDDayOfWeek(int group, int dayOfWeek, RequestDatabaseShedComplete requestDatabaseShedComplete) {
        //в отдельном потоке посылаем запрос в бд
        AsyncTask.execute(() -> {
            //получаем текущее состояние бд
            AppDatabase db = App.getInstance().getDatabase();
            ShedDao shedDao=db.shedDao();
            //получаем данные с соотвествующего запроса
            ArrayList<ItemShed> itemShed= (ArrayList<ItemShed>) shedDao.getItemShedForThisGroupAndDayOfWeek(group,dayOfWeek);
            //отправка в соответсвующий callback по завершению работы потока
            requestDatabaseShedComplete.getItemShedsCompleteForDayOfWeekANDThisGroup(itemShed);
        });
    }


}
