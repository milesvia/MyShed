package com.example.myapplication.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;
import static android.content.Intent.FLAG_GRANT_WRITE_URI_PERMISSION;

public class FileUtil {
    public FileUtil(){    }

    //открытие окна Менеджер файлов для выбора своего exel файла для парсинга в приложение
    public void openManagerFiles(OpenManagerFileComplete openManagerFileComplete) {
        Intent intent = new Intent()
                .setType("*/*")
                .setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setFlags(FLAG_GRANT_READ_URI_PERMISSION | FLAG_GRANT_WRITE_URI_PERMISSION);
        //открываем менеджер файлов и ставим флажок requestCode=123, который поможет определить, какое окно закрылось
        openManagerFileComplete.openManagFiles(intent);//
    }


}
