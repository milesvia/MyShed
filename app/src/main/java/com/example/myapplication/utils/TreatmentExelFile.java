package com.example.myapplication.utils;

import android.content.Context;
import android.view.View;

import com.example.myapplication.database.enity.ItemShed;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class TreatmentExelFile {
    public TreatmentExelFile(){

    }
    public   List<ItemShed> openAndReadMainExelFile(InputStream stream) {
        try {
            ArrayList<ItemShed> thisShed=new ArrayList<>();
            XSSFWorkbook workbook = new XSSFWorkbook(stream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            int rowsCount = sheet.getPhysicalNumberOfRows();
            FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
            for (int r = 0; r<rowsCount; r++) {
                Row row = sheet.getRow(r);
                ItemShed itemShed=new ItemShed();
                itemShed.setTime(getCellAsString(row, 0, formulaEvaluator));
                itemShed.setObject(getCellAsString(row, 1, formulaEvaluator));
                itemShed.setAddress(getCellAsString(row, 2, formulaEvaluator));
                itemShed.setTeacher(getCellAsString(row, 3, formulaEvaluator));
                itemShed.setGroup(Integer.parseInt(getCellAsString(row, 4, formulaEvaluator)));
                itemShed.setDayOfWeek(numberDayOfWeek(getCellAsString(row, 5, formulaEvaluator)));
                System.out.println(getCellAsString(row, 5, formulaEvaluator)+numberDayOfWeek((getCellAsString(row, 5, formulaEvaluator))));
                thisShed.add(itemShed);
            }
            return thisShed;
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return null;
    }

    private static int numberDayOfWeek(String dayOfWeek){
        final String[] weeksDay = { "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота" };
        for(int i=0;i<weeksDay.length;i++){
            if(dayOfWeek.compareTo(weeksDay[i])==0)
                return i;
        }
        return -1;
    }

    protected static String getCellAsString(Row row, int c, FormulaEvaluator formulaEvaluator) {
        String value = "";
        if (row != null) {
            try {
                Cell cell = row.getCell(c);
                CellValue cellValue = formulaEvaluator.evaluate(cell);
                switch (cellValue.getCellType()) {
                    case Cell.CELL_TYPE_BOOLEAN:
                        value =  String.valueOf(cellValue.getBooleanValue()).trim();
                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        value  = String.valueOf((int) cellValue.getNumberValue());
                        break;
                    default:
                        value = String.valueOf(cellValue.getStringValue()).trim();
                }
            } catch(NullPointerException e){
                System.out.println("ERROR: "+e.toString());
            }}
        return value;
    }
}
